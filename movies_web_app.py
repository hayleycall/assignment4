import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = "CREATE TABLE mov_info (id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating TEXT, PRIMARY KEY (id))"

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


@app.route('/add_movie', methods=['POST'])
def insert_movie_to_db():
    year_arg = request.form['year']
    title_arg = request.form['title']
    director_arg = request.form['director']
    actor_arg = request.form['actor']
    release_arg= request.form['release_date']
    rating_arg = request.form['rating']

    # per piazza, want to do all checks case-insensitve so I'm just making everything lowercase
    # also can assume all fields will be used when inserting movie
    year_arg = year_arg.lower()
    title_arg = title_arg.lower()
    director_arg = director_arg.lower()
    actor_arg = actor_arg.lower()
    release_arg = release_arg.lower()
    rating_arg = rating_arg.lower()



    db, username, password, hostname = get_db_creds()
    msg = ''
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    ## check to see if the movie exists first
    try:
        sql_query = "SELECT * FROM mov_info WHERE title = " + "\'" + str(title_arg) + "\'"
        cur.execute(sql_query)
        check = cur.fetchall()

       
        if len(check) != 0:
            msg= "Movie " + str(title_arg) + " could not be inserted because it already exists, please use update instead"
            return render_template('index.html', message = msg)
    # if the check for existance errors out, force user to try again
    except Exception as exp:
        msg = "Movie " + str(title_arg) + " could not be inserted - " + str(exp)
        return render_template('index.html', message = msg)
    try:
        sql_query = "INSERT INTO mov_info (year, title, director, actor, release_date, rating) values (%s, %s, %s, %s, %s, %s)"
        data = (year_arg, title_arg, director_arg, actor_arg, release_arg, rating_arg)
        cur.execute(sql_query, data)
        cnx.commit()
        msg = "Movie " + str(title_arg) + " sucessfully inserted"
        return render_template('index.html', message = msg)
    except Exception as exp:
        msg = "Movie " + str(title_arg) + " could not be inserted - " + str(exp)
    return render_template('index.html', message = msg)


@app.route('/update_movie', methods=['POST'])
def update_movie_to_db():
    year_arg = request.form['year']
    title_arg = request.form['title']
    director_arg = request.form['director']
    actor_arg = request.form['actor']
    release_arg= request.form['release_date']
    rating_arg = request.form['rating']
    # per assignment file, the user will insert all fields when wanting to update
    # per piazza, want to do all checks case-insensitve so I'm just making everything lowercase
    year_arg = year_arg.lower()
    title_arg = title_arg.lower()
    director_arg = director_arg.lower()
    actor_arg = actor_arg.lower()
    release_arg = release_arg.lower()
    rating_arg = rating_arg.lower()



    db, username, password, hostname = get_db_creds()
    msg = ''
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    ## check to see if the movie does not exist first
    try:
        sql_query = "SELECT * FROM mov_info WHERE title = " + "\'" + str(title_arg) + "\'"
        cur.execute(sql_query)
        check = cur.fetchall()
        if len(check) == 0:
            msg= "Movie " + str(title_arg) + " could not be updated because it does not exist, please use insert instead"
            return render_template('index.html', message = msg)
    # if the check for existance errors out, force user to try again
    except Exception as exp:
        msg = "Movie " + str(title_arg) + " could not be updated - " + str(exp)
        return render_template('index.html', message = msg)

    try:
        sql_query = "UPDATE mov_info SET year = %s, title = %s, director = %s, actor = %s, release_date = %s, rating = %s WHERE title = " + "\'" + str(title_arg) + "\'"
        data = (year_arg, title_arg, director_arg, actor_arg, release_arg, rating_arg)
        cur.execute(sql_query, data)
        cnx.commit()
        msg += "Movie " + str(title_arg) + " sucessfully updated"
        return render_template('index.html', message = msg)        
    except Exception as exp:
        msg = "Movie " + str(title_arg) + " could not be updated - " + str(exp)
        return render_template('index.html', message = msg)     

@app.route('/delete_movie', methods=['POST'])
def delete_movie_from_db():
    ## per assignment file, can assume only title is given
    title_arg = request.form['delete_title']
    # per piazza, want to do all checks case-insensitve so I'm just making everything lowercase
    title_arg = title_arg.lower()

    db, username, password, hostname = get_db_creds()
    msg = ''
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    ## check to see if the movie does not exist first
    try:
        sql_query = "SELECT * FROM mov_info WHERE title = " + "\'" + str(title_arg) + "\'"
        cur.execute(sql_query)
        check = cur.fetchall()
        if len(check) == 0:
            msg= "Movie with " + str(title_arg) + " does not exist"
            return render_template('index.html', message = msg)
    # if the check for existance errors out, force user to try again
    except Exception as exp:
        msg = "Movie " + str(title_arg) + " could not be deleted - " + str(exp)
        return render_template('index.html', message = msg)

    try:
        sql_query = "DELETE FROM mov_info WHERE title = " + "\'" + str(title_arg) + "\'"
        cur.execute(sql_query)
        cnx.commit()
        msg += "Movie " + str(title_arg) + " sucessfully deleted"
        return render_template('index.html', message = msg)        
    except Exception as exp:
        msg = "Movie " + str(title_arg) + " could not be deleted - " + str(exp)
        return render_template('index.html', message = msg)     

@app.route('/search_movie', methods=['GET'])
def search_movie_in_db():
    ## per assignment file, can assume only actor is given
    actor_arg = request.args.get('search_actor')
    # per piazza, want to do all checks case-insensitve so I'm just making everything lowercase
    actor_arg = actor_arg.lower()

    db, username, password, hostname = get_db_creds()
    msg = ''
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    ## check to see if the movie does not exist first
    try:
        sql_query = "SELECT title, year, actor FROM mov_info WHERE actor = " + "\'" + str(actor_arg) + "\'"
        cur.execute(sql_query)
        check = cur.fetchall()
        if len(check) == 0:
            msg = "No movies found for actor " + str(actor_arg) 
            return render_template('index.html', message = msg)
        else: 
            for movie in check:
                msg += "<br>" + str(movie[0]) + ", "
                msg += str(movie[1]) + ", "
                msg += str(movie[2]) + "</br>"
            return render_template('index.html', message = msg)

    except Exception as exp:
        msg = "Search for" + str(actor_arg) + " could not be completed - " + str(exp)
        return render_template('index.html', message = msg)

@app.route('/highest_rating', methods=['GET'])
def print_highest_rating__movies_in_db():
    db, username, password, hostname = get_db_creds()
    msg = ''
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    ## check to see if a movie exists first
    try:
        sql_query = "SELECT MAX(rating) FROM mov_info"
        cur.execute(sql_query)
        check = cur.fetchall()
        if len(check) == 0:
            msg = "No movies in table"
            return render_template('index.html', message = msg)
        else: 
            for movie in check:
                highest_rating = str(movie[0])
            sql_query = "SELECT title, year, actor, director, rating FROM mov_info WHERE rating = " + "\'" + highest_rating + "\'"
            cur.execute(sql_query)
            check = cur.fetchall()
            for movie in check:
                msg += "<br>" + str(movie[0]) + ", "
                msg += str(movie[1]) + ", "
                msg += str(movie[2]) + ", "
                msg += str(movie[3]) + ", "
                msg += str(movie[4]) + "</br>"
            return render_template('index.html', message = msg)

    except Exception as exp:
        msg = "Printing highest ratings could not be completed - " + str(exp)
        return render_template('index.html', message = msg)

@app.route('/lowest_rating', methods=['GET'])
def print_lowest_rating__movies_in_db():
    db, username, password, hostname = get_db_creds()
    msg = ''
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    ## check to see if a movie exists first
    try:
        sql_query = "SELECT MIN(rating) FROM mov_info"      
        cur.execute(sql_query)
        check = cur.fetchall()
        if len(check) == 0:
            msg = "No movies in table"
            return render_template('index.html', message = msg)
        else: 
            for movie in check:
                lowest_rating = str(movie[0])
            sql_query = "SELECT title, year, actor, director, rating FROM mov_info WHERE rating = " + "\'" + lowest_rating + "\'"
            cur.execute(sql_query)
            check = cur.fetchall()
            for movie in check:
                msg += "<br>" + str(movie[0]) + ", "
                msg += str(movie[1]) + ", "
                msg += str(movie[2]) + ", "
                msg += str(movie[3]) + ", "
                msg += str(movie[4]) + "</br>"
            return render_template('index.html', message = msg)

    except Exception as exp:
        msg = "Printing highest ratings could not be completed - " + str(exp)
        return render_template('index.html', message = msg)
  


@app.route("/")
def hello():
    create_table()
    return render_template('index.html')



if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
