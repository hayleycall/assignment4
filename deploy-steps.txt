echo "This is the deploy step"
export PROJECT_ID=assignment4-234819
gcloud config set project $PROJECT_ID
export CLOUDSDK_COMPUTE_ZONE=us-central1-b
cd ../assignment4
gcloud container clusters get-credentials testcluster1
kubectl delete deployment movies-deployment || echo "movies-deployment deployment does not exist"
kubectl delete service movies-deployment || echo "movies-deployment service does not exist"
kubectl delete ingress movies-ingress || echo "movies-ingress does not exist"
kubectl create -f movies-deployment.yaml
kubectl expose deployment movies-deployment --target-port=5000 --type=NodePort 
kubectl apply -f movies-ingress.yaml
echo "Done deploying"